# Chapter 1: Operators

Use the off the shelf middleware when possible, only care about the low level details when it's not avoidable, such as you need to implement operators on your own chip.

## 1. Use Off-the-shelf midware

* Nvidia: CUBLAS, CUDNN
* Intel: MKL
* Others...

The bright side is : These libs are written by the hardware vendors to prompt their products and to add addtional values to the machine as a solution not just a metal hardware. And it's highly optimized by the experts who have much deeper knowledge of the machine, greater internal tools for optimization, strong motivation to chase high peformance goals.

But there are following drawbacks, those may major issue or minor depending on the targets, requirements, budgets of the framework software.

1. The optimization is always workload originated, so it can be outdated and always behind the development the frontier of algorithem.
2. The APIs are designed by different vendors than the framework developers, needs integration effort and may have sharp learning curve.
3. Implicit knowledge which may not be perfectly expressed by the public documents of these libs.
4. Hidden bugs needs great effort to repro, debug and fix, depends on the support of these libs promised.

### 1.1 Nvidia CUDA Libs

1. cuBLAS Supporting multiple precisions, and TensorCore acceleration.
2. cudnn Support both forward and backword of many ops in the DL networks. The cuDNN library exposes a Host API but assumes that for operations using the GPU, the necessary data is directly accessible from the device.

### 1.2 Intel MKL-DNN Libs

Or it's called DNNL, oneDNN, [https://github.com/oneapi-src/oneDNN](https://github.com/oneapi-src/oneDNN). Marketing terms and document are so confusing, mixing and old and new terms, old/new links everywhere. What's surprised me is it, the oneDNN claims to support, ARM/NVIDIA/IBM chips now. See [https://github.com/oneapi-src/oneDNN/tree/master/src/gpu/nvidia](https://github.com/oneapi-src/oneDNN/tree/master/src/gpu/nvidia) for NVIDIA GPU backend. These are just a wrapper against the cudnn lib. For other GPUs supports, it uses openCL language.

* Programming model of oneDNN. ![Programming model](../.gitbook/assets/img_programming_model.png) A primitive is a functor, an Engine is a device on the system, it's either CPU or GPU. Memory object is the encapsulate handles to the memory allocated in an Engine, includeing the layout, size infomormation.
* Supporting data types of the oneDNN
  * fp16, bf16, fp32, u8/s8

    Intel CPU supports the bf16 by the "DL Boost" market term, while fp16 is not supported.

    While INTEL GPU support fp16.

### 1.3 Other libs

## 2. Generated kernels by tools

There tools target to generate kernels automatically, like the following ones.

* TVM
* XLA
* Ad-hoc kernel generater

### 2.1 TVM

Quick start guide to use the TVM Tensor Expression: [https://tvm.apache.org/docs/tutorials/get\_started/tensor\_expr\_get\_started.html](https://tvm.apache.org/docs/tutorials/get_started/tensor_expr_get_started.html)

Pros:

* High level
  * No need to hand craft kernels, which maybe painful or no gain, depends on the human skills and vendor languages/tools.
* Support multiple backends already
  * No hardware vendor lock-in, software vendor reuse the TVM languages
* Open source, debugable, and hackable.

Cons:

* TVM DSL, sharp learning curve of the domain specific knowledge ?
  * New abstraction levels, other than the machine model and the C familiy languages.
  * compute, scedule, context, placeholder, op
* Needs to be **a compiler experts to get highly optimized result**?
  * The compiler is trying to be generic enough to solve problems, to abstract aways the low level details, but the compiler itself may become a complex monster until a point?
  * No school education of this?
* Still need low level knowledge of the machine/backend ?
  * Like, threadIdx.x in shedule bind?
* Integration effort ?
  * Store a pre-defined set of generated kernels or always JIT on the fly?
* Unmature tools?
  * 

### 2.2 XLA

Pros:

Cons:

### 2.3 Ad-hoc kernel generators

* Tensorflow
* Pytorch
* MxNet

## Hand crafted kernels

Why? New hardware? New data types? Domain specific information

